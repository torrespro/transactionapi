# Transactions API using Spring boot and Camel's REST DSL

### Introduction
This app uses Camel's REST DSL and Spring Boot
to expose a RESTful API which provides these operations:

- GET transactionservice/transaction/{transaction_id}     - Find transaction by id
- PUT transactionservice/transaction/{transaction_id}     - Create transaction
- GET transactionservice/sum/{transaction_id}             - sum of all transactions that are transitively linked by their parent_id to $transaction_id    
- GET transactionservice/types/{type}                     - Find transactions by type

It relies on Swagger to expose the API documentation of the REST service.

The app is packaged as a fat WAR. Fat WARs can be executed just as regular fat jars, but you can also deploy them to the servlet containers like Tomcat. 

### Prerequisites
Java 1.8 (for the use of streams)

### Build
You will need to compile this example first:

```sh
$ mvn clean install
```

### Run

You can run this example with Maven using:

```sh
$ mvn spring-boot:run
```

Alternatively, you can also run this example using the executable WAR:

```sh
$ java -jar target/transactionsAPI.war
```

### Swagger API

The API documentation of the service using Swagger using
the _context-path_ `transactionservices/api-doc`. You can access the API documentation
from your Web browser at <http://localhost:8080/transactionservice/api-doc>.

(YAML also available)

<http://localhost:8080/transactionservice/api-doc/swagger.yaml>

### Asymptotic behaviour Comments
Using maps and saving the list of children as a list inside each Transactions so:

- Put and Get operations are constant O(1) (assuming the the hash function disperses elements properly among the buckets. Worst case scenario O(n)).
- Sum operation because we access directly to the list of children is constant too.
- Filter by type requires by HashMap definition: time proportional to the "capacity" of the HashMap 
instance (the number of buckets) plus its size (the number of key-value mappings).
Using parallel streams from java 8 to make it faster.