package com.torres.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.torres.model.Transaction;
import com.torres.processors.AddTransactionProcessor;
import com.torres.processors.GetTransactionProcessor;
import com.torres.processors.SumTransactionProcessor;

import static org.apache.camel.model.rest.RestParamType.body;
import static org.apache.camel.model.rest.RestParamType.path;

/**
 * * Created by andres on 25/10/16.
 *
 * Camel routes to create and retrieve transactions
 * <p/>
 * Use <tt>@Component</tt> to make Camel auto detect this route when starting.
 * Camel context is created automatically by camel-spring-boot
 */
@Component
public class TransactionsCamelRouter extends RouteBuilder {

    @Autowired
    private AddTransactionProcessor addTransactionProcessor;

    @Autowired
    private GetTransactionProcessor getTransactionProcessor;

    @Autowired
    private SumTransactionProcessor sumTransactionProcessor;

    @Override
    public void configure() throws Exception {

//        Configures the REST DSL to use a specific REST implementation (Restlet).
        restConfiguration()
                // add swagger api-doc out of the box
                .apiContextPath("/api-doc")
                .apiProperty("api.title", "Transactions API").apiProperty("api.version", "1.0")
                // and enable CORS
                .apiProperty("cors", "true")
                .bindingMode(RestBindingMode.json);

        // this transactions REST service is json only
        rest().description("Transactions rest service")
                .consumes("application/json").produces("application/json")
                .get("/transaction/{transaction_id}").description("Find transaction by id")
                .outType(Transaction.class).id("getTransaction")
                .param().name("transaction_id").type(path).description("The id of the transaction to get").dataType("integer").endParam()
                .responseMessage().code(200).message("The transaction").endResponseMessage()
                .to("direct:getTransaction")

                .put("/transaction/{transaction_id}").description("Creates a transaction")
                .type(Transaction.class).id("putTransaction")
                .param().name("transaction_id").type(path).description("The id of the transaction to create").dataType("integer").endParam()
                .param().name("body").type(body).description("The transaction to create").endParam()
                .responseMessage().code(200).message("transaction created").endResponseMessage()
                .to("direct:addTransaction")

                .get("/sum/{transaction_id}").description("A sum of all transactions that are transitively linked by their parent_id to $transaction_id.")
                .id("getSumTransaction")
                .param().name("transaction_id").type(path).description("The id of the transaction to get").dataType("integer").endParam()
                .responseMessage().code(200).message("Sum of the transaction amount and of all transactions that are transitively linked").endResponseMessage()
                .to("direct:sumTransaction")

                .get("/types/{type}").description("Find all transaction per type")
                .outTypeList(Transaction.class).id("getTransactionsPerType")
                .param().name("type").type(path).description("The type of transactions to get").dataType("string").endParam()
                .responseMessage().code(200).message("All transactions from the same type").endResponseMessage()
                .to("direct:getTransactionsPerType");

        from("direct:addTransaction")
                .process(addTransactionProcessor);

        from("direct:getTransaction")
                .process(getTransactionProcessor);

        from("direct:sumTransaction")
                .process(sumTransactionProcessor);

        from("direct:getTransactionsPerType")
                .bean("transactionService", "getTransactionPerType(${header.type})");
    }

}
