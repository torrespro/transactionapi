package com.torres.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.torres.service.TransactionService;

import java.util.HashMap;

/**
 * Created by andres on 27/10/16.
 */
@Component
public class SumTransactionProcessor implements Processor {

    @Autowired
    TransactionService transactionService;

    public void process(Exchange exchange) throws Exception {

        String id = (String) exchange.getIn().getHeader("transaction_id");
        Double sum = transactionService.sumTransaction(id);
        //build response
        HashMap<String, Double> ok = new HashMap();
        ok.put("sum", sum);
        exchange.getOut().setBody(ok);
    }
}
