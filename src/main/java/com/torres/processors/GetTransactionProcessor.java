package com.torres.processors;

import com.torres.model.Transaction;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.torres.service.TransactionService;

/**
 * Created by andres on 27/10/16.
 */
@Component
public class GetTransactionProcessor implements Processor {

    @Autowired
    TransactionService transactionService;

    public void process(Exchange exchange) throws Exception {

        String id = (String) exchange.getIn().getHeader("transaction_id");
        Transaction transaction = transactionService.getTransaction(id);
        if(transaction != null)
        {
            exchange.getOut().setBody(transaction);
        }
    }
}
