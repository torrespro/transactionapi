package com.torres.processors;

import com.torres.model.Transaction;
import com.torres.service.TransactionService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by andres on 25/10/16.
 */
@Component
public class AddTransactionProcessor implements Processor {

    @Autowired
    TransactionService transactionService;

    public void process(Exchange exchange) throws Exception {

        Transaction transaction = (Transaction) exchange.getIn().getBody();
        String id = (String) exchange.getIn().getHeader("transaction_id");

        transactionService.addTransaction(id, transaction);

        //build response
        HashMap<String, String> ok = new HashMap();
        ok.put("status", "ok");
        exchange.getOut().setBody(ok);
    }
}
