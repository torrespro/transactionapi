package com.torres.service;

import com.torres.StorageBean;
import com.torres.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * * Created by andres on 25/10/16.
 *
 * Service class with the business logic related to transactions data, calculations, etc
 */
@Service
public class TransactionService {

    @Autowired
    StorageBean storageBean;

    public void addTransaction(String id, Transaction transaction) {
        if (transaction.getParent_id() != null) {
            addChild(transaction.getParent_id(), Long.parseLong(id));
        }
        storageBean.getStorage().put(Long.parseLong(id), transaction);
    }

    public Transaction getTransaction(String id) {

            return storageBean.getStorage().get(Long.parseLong(id));
    }

    public Transaction[] getTransactionPerType(String type) {
        Collection<Transaction> transactions = storageBean.getStorage().values();
        // Convert it into a Stream
        final Stream<Transaction> myStream = transactions.stream().parallel();
        return myStream.filter(s -> type.equalsIgnoreCase(s.getType()))
                .toArray(Transaction[]::new);
    }

    public void addChild(Long id, Long childId) {
        Transaction transaction = storageBean.getStorage().get(id);
        if (transaction != null) {
            transaction.getChildren().add(childId);
        }

    }

    public Double sumTransaction(String id) {
        Transaction transaction = storageBean.getStorage().get(Long.parseLong(id));
        Double sum = 0.0;
        if (transaction != null) {
            sum = transaction.getAmount();
            List<Long> list = transaction.getChildren();
            for (int i = 0; i < list.size(); i++) {
                Transaction transactionChild = storageBean.getStorage().get(list.get(i));
                sum += transactionChild.getAmount();
            }
        }

        return sum;
    }
}
