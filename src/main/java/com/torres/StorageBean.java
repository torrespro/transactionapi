/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.torres;

import com.torres.model.Transaction;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * * Created by andres on 25/10/16.
 *
 * A bean that save transactions data temporary in memory
 * <p/>
 * Uses <tt>@Component</tt> to register this bean with the name <tt>storageBean</tt>
 * that we use in the Camel route to lookup this bean.
 */
@Component
public class StorageBean {

    private Map<Long, Transaction> storage = new HashMap<Long, Transaction>();

    public Map<Long, Transaction> getStorage() {
        return storage;
    }

    public void setStorage(Map<Long, Transaction> storage) {
        this.storage = storage;
    }
}
