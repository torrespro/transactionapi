/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.torres;

import com.torres.model.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CamelApplicationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void init() {
        Transaction transaction10 = new Transaction();
        transaction10.setType("cars");
        transaction10.setAmount(5000.0);

        Transaction transaction11 = new Transaction();
        transaction11.setType("shopping");
        transaction11.setAmount(10000.0);
        transaction11.setParent_id(new Long(10));

        Transaction transaction12 = new Transaction();
        transaction12.setType("shopping");
        transaction12.setAmount(1000.0);

        HttpEntity<Transaction> request = new HttpEntity<>(transaction10);
        restTemplate.put("/transactionservice/transaction/10", request);
        request = new HttpEntity<>(transaction11);
        restTemplate.put("/transactionservice/transaction/11", request);
        request = new HttpEntity<>(transaction12);
        restTemplate.put("/transactionservice/transaction/12", request);
    }

    @Test
    public void addTransaction() throws IOException {

        Transaction transaction1 = new Transaction();
        transaction1.setType("test");
        transaction1.setAmount(1000.0);

        HttpEntity<Transaction> request = new HttpEntity<>(transaction1);

        ResponseEntity<Map> response = restTemplate.exchange("/transactionservice/transaction/1",
                HttpMethod.PUT, request, new ParameterizedTypeReference<Map>() {
                });

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Map status = response.getBody();
        assertThat(status.get("status")).isEqualTo("ok");


    }

    @Test
    public void getTransaction() {

        // Then call the REST API
        ResponseEntity<Transaction> response = restTemplate.getForEntity("/transactionservice/transaction/10", Transaction.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Transaction transaction = response.getBody();
        assertThat(transaction.getAmount()).isBetween(1.0, 10000.0);
        assertThat(transaction.getType()).isIn("shopping", "cars");
    }

    @Test
    public void getSum() {

        // Then call the REST API
        ResponseEntity<Map> response = restTemplate.getForEntity("/transactionservice/sum/10", Map.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Map transaction = response.getBody();
        assertThat(transaction.get("sum")).isEqualTo(15000.0);
    }

    @Test
    public void getTransactionsPerType() {
        ResponseEntity<ArrayList<Map>> response = restTemplate.exchange("/transactionservice/types/shopping",
                HttpMethod.GET, null, new ParameterizedTypeReference<ArrayList<Map>>() {
                });
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        ArrayList<Map> transactionList = response.getBody();
        assertThat(transactionList).hasSize(2);
        assertThat(transactionList.get(0).get("type").equals("shopping"));
        assertThat(transactionList.get(1).get("type").equals("shopping"));
    }

}